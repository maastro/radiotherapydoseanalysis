# README #

 This library was written for Matlab 2015b and was tested for this version. Newer versions should be verified by the user.

### What is this repository for? ###

* Converting DICOM RT objects into DoseVolumeHistograms
* Interface with JAVA to run calculations in any JAVA framework using the Matlab Compiler NE

### How do I get set up? ###

* Download or Clone/Fork the repository;
* Add [Matlab Dependency Manager](https://bitbucket.org/maastroclinic/matlab-dependency-manager.git) to your Matlab path;
* run **"installDependencies(** *fullRepositoryRootPath* **)"**
* if you have already installed it but are cloning a new version run **"updateDependencies(** *fullRepositoryRootPath* **)"** instead;
* run **resetPath()**
* run **"setPathForUse(** *fullRepositoryRootPath* **)"**

### How do I verify my library? ###
* download the DICOM data accompanied with this repository (see downloads)
* unzip the DICOM data in the resources folder
* run **resetPath()**
* run **"setPathForTesting(** *fullRepositoryRootPath* **)"**
* run **runAllTest.m**

* if all tests are successful the library works for your system and it is ready for use!
* if you run into trouble check any externalLibs for tests and try to run them! 

### Contribution guidelines ###

* Use the *prototype* folders to save functions that are not verified/tested but can be useful for others
* Please fork this repository when wanting to make adjustments.
* To keep the code readable and similar please consult the **codingStandards.md** included with this repository when contributing 

### Who do I talk to? ###

* MAASTRO Clinic Software Development
* Tim Lustberg 
	* tim.lustberg@maastro.nl