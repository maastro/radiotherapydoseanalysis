classdef JavaInterfaceTest < matlab.unittest.TestCase
    %JAVAINTERFACETEST Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        STRUCT_FILE = './resources/maastro/RTSTRUCT.9999.70989091007225463773458855903144372742.dcm';
        DOSE_FILE = './resources/maastro/RTDOSE.9999.245420512207561792050922341714899059838.dcm';
        PLAN_FILE = './resources/maastro/RTPLAN.9999.42722492099431225192788770082885116854.dcm'
        TEST_CT_SCAN_DIR = './resources/maastro/ct/'
        ROI_NAME = 'GTV-1';
        ROI_NAMES = {'GTV-1', 'GTV-2'};
        ROI_NAMES_OPERATORS = {'+'}
        ROI_NAMES_OPERATORS_INVALID = {'42'}
        BIN_SIZE = 0.01;
        
        REF_VOLUME_CONTOUR = 58.2733;
        REF_MEAN_DOSE = 48.3739;
        
        ctScan
        ctImage
        rtdose
        rtstruct
        refVoi
        refVoiCombined
    end
    
    methods (TestClassSetup)
        function setupOnce(this)
            this.ctScan = CtScan(this.TEST_CT_SCAN_DIR);
            this.ctImage = createImageFromCtScan(this.ctScan);
            this.rtdose = RtDose(DicomObj(this.DOSE_FILE));
            this.rtdose = this.rtdose.readDicomData();

            this.rtstruct = RtStruct(DicomObj(this.STRUCT_FILE));
            contour = createContour(this.rtstruct, this.ROI_NAME);
            this.refVoi = createVolumeOfInterest(contour, this.ctImage);
            
            contour = createContour(this.rtstruct, this.ROI_NAMES{2});
            this.refVoiCombined = createVolumeOfInterest(contour, this.ctImage);
            this.refVoiCombined = this.refVoiCombined + this.refVoi;
        end
    end
    
    methods (Test)
        %--- happy path ---%
        function createCtScanTest(this)
            files = rdir(fullfile(this.TEST_CT_SCAN_DIR, '**', '*'));
            javaCtScan = createCtScan({files.name});
            this.ctScan = this.ctScan.readDicomData();
            verifyEqual(this, javaCtScan, this.ctScan);
        end
        
        function createImageFromCtPropertiesTest(this)
            image = createImageFromCtProperties(mockJavaCtProperties(this.TEST_CT_SCAN_DIR));
            refImage = createImageFromCtScan(this.ctScan);
            verifyEqual(this, image, refImage);
        end
        
        function createRtStructTest(this)
            createRtStruct(this.STRUCT_FILE);
        end
        
        function createRtDoseTest(this)
            createRtDose(this.DOSE_FILE);
        end
        
        function createReferenceDoseTest(this)
            image = createReferenceDose(this.DOSE_FILE, this.ctImage);
            refImage = createImageFromRtDose(this.rtdose);
            refImage = matchImageRepresentation(refImage, this.ctImage);
            verifyEqual(this, image, refImage);
        end
        
        function combineVoisSingleVoiTest(this)
            voi = combineVois( this.rtstruct, {this.ROI_NAME}, {''}, this.ctImage);
            verifyEqual(this, voi, this.refVoi);
        end
        
        function combineVoisTest(this)
            voi = combineVois( this.rtstruct, this.ROI_NAMES, this.ROI_NAMES_OPERATORS, this.ctImage);
            verifyEqual(this, voi, this.refVoiCombined);
        end
        
        function createDoseVolumeHistogramTest(this)
            dvhJson = createDoseVolumeHistogram(this.rtstruct, ...
                createReferenceDose(this.DOSE_FILE, this.ctImage), ...
                this.ctImage, ...
                {this.ROI_NAME}, ...
                {''}, ...
                this.BIN_SIZE);
            dvhFromJson = loadjson(dvhJson);
            verifyEqual(this, dvhFromJson.volume, this.REF_VOLUME_CONTOUR, 'RelTol', 0.001);
            verifyEqual(this, dvhFromJson.meanDose, this.REF_MEAN_DOSE, 'RelTol', 0.001);
        end
        
        function createDoseVolumeHistogramNoDoseTest(this)
            dvhJson = createDoseVolumeHistogramNoDose(this.rtstruct, ...
                this.ctImage, ...
                {this.ROI_NAME}, ...
                {''});
            dvhFromJson = loadjson(dvhJson);
            verifyEqual(this, dvhFromJson.volume, this.REF_VOLUME_CONTOUR, 'RelTol', 0.001);
        end
        
        function createDoseVolumeHistogramUsingVoiTest(this)
            dvhJson = createDoseVolumeHistogramUsingVoi(this.refVoi, ...
                createReferenceDose(this.DOSE_FILE, this.ctImage), ...
                this.BIN_SIZE);
            dvhFromJson = loadjson(dvhJson);
            verifyEqual(this, dvhFromJson.volume, this.REF_VOLUME_CONTOUR, 'RelTol', 0.001);
            verifyEqual(this, dvhFromJson.meanDose, this.REF_MEAN_DOSE, 'RelTol', 0.001);
        end
        
        function createDoseVolumeHistogramUsingVoiNoDoseTest(this)
            dvhJson = createDoseVolumeHistogramUsingVoiNoDose(this.refVoi);
            dvhFromJson = loadjson(dvhJson);
            verifyEqual(this, dvhFromJson.volume, this.REF_VOLUME_CONTOUR, 'RelTol', 0.001);
        end
        
        %--- unhappy path ---%
        function createCtScanInvalidInputTest(this)
            verifyError(this, @()createCtScan('wrongInputFormat'), 'createCtScan:InvalidInput');
        end
        
        function createImageFromCtPropertiesInvalidInputTest(this)
            someStruct.a = 1;
            verifyError(this,  @()createImageFromCtProperties(someStruct), 'createImageFromCtProperties:validateCtProperties:InvalidInput');
            verifyError(this, @()createImageFromCtProperties(mockJavaCtPropertiesInvalidInput()), 'createImageFromCtProperties:validateCtProperties:InvalidInput');
        end
        
        function combineVoisInvalidInputTest(this)
            verifyError(this, @()combineVois( this.rtstruct, this.ROI_NAMES, this.ROI_NAMES_OPERATORS_INVALID, this.ctImage), 'combineVois:ProcessingError');
        end
    end
    
end

