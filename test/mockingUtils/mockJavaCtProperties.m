function javaCtProperties = mockJavaCtProperties(folder)
    files = rdir(fullfile(folder, '**', '*'));

    header = dicominfo(files(1).name);
    for file = files';
        nextHeader = dicominfo(file.name);
        if header.ImagePositionPatient(3) > nextHeader.ImagePositionPatient(3);
            header = nextHeader;
        end
    end

    javaCtProperties.PixelSpacing = header.PixelSpacing';
    javaCtProperties.SliceThickness =  header.SliceThickness;
    javaCtProperties.Rows =  header.Rows;
    javaCtProperties.Columns =  header.Columns;
    javaCtProperties.ImageOrientationPatient =  header.ImageOrientationPatient';
    javaCtProperties.ImagePositionPatient =  header.ImagePositionPatient';
    javaCtProperties.CTFileLength =  length(files);
end