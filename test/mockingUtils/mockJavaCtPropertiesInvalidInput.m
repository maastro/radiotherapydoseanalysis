function javaCtProperties = mockJavaCtPropertiesInvalidInput()
    javaCtProperties.PixelSpacing = 'wrong!';
    javaCtProperties.SliceThickness = 'wrong!';
    javaCtProperties.Rows = 'wrong!';
    javaCtProperties.Columns = 'wrong!';
    javaCtProperties.ImageOrientationPatient = 'wrong!';
    javaCtProperties.ImagePositionPatient = 'wrong!';
    javaCtProperties.CTFileLength = 'wrong!';
end