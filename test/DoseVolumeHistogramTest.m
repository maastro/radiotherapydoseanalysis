classdef DoseVolumeHistogramTest < matlab.unittest.TestCase
    %DOSEVOLUMEHISTOGRAMTEST test all basic functionality of the dose volume histogram.
    
    properties
        STRUCT_FILE = './resources/maastro/RTSTRUCT.9999.70989091007225463773458855903144372742.dcm';
        DOSE_FILE = './resources/maastro/RTDOSE.9999.245420512207561792050922341714899059838.dcm';
        PLAN_FILE = './resources/maastro/RTPLAN.9999.42722492099431225192788770082885116854.dcm'
        TEST_CT_SCAN_DIR = './resources/maastro/ct/'
        ROI_NAME = 'GTV-1';
        
        REF_VOLUME_CONTOUR = 58.2733;
        REF_VOLUME_INDEX = 4876;
        REF_VOLUME_VALUE = 20.2274;
        REF_V_PARAM = 40.9956; %cc
        REF_V_PARAM_REL = 70.3505; %Perc
        REF_V_LIMIT = 48; %Gy
        REF_D_PARAM = 49.92; %Gy
        REF_D_PARAM_REL = 110.9333; %Perc
        REF_D_LIMIT_REL = 2; %Perc
        BIN_SIZE = 0.01;
        
        voi;
        refDose;
        targetPrescriptionDose;
    end
    
    methods (TestClassSetup)
        function setupOnce(this)
            ctScan = CtScan(this.TEST_CT_SCAN_DIR);
            refImage = createImageFromCtScan(ctScan);
            rtdose = RtDose(DicomObj(this.DOSE_FILE));
            rtdose = rtdose.readDicomData();
            doseImage = createImageFromRtDose(rtdose);
            this.refDose = matchImageRepresentation(doseImage, refImage);
            
            rtstruct = RtStruct(DicomObj(this.STRUCT_FILE));
            contour = createContour(rtstruct, this.ROI_NAME);
            this.voi = createVolumeOfInterest(contour, refImage);
                        
            rtplan = RtPlan(DicomObj(this.PLAN_FILE));
            this.targetPrescriptionDose = rtplan.targetPrescriptionDose;
        end
    end
    
    methods(Test)
        %--- happy path ---%
        function doseVolumeHistogramTest(this)
            dvh = DoseVolumeHistogram(this.refDose, this.voi, this.BIN_SIZE);
            verifyEqual(this, dvh.vVolume(this.REF_VOLUME_INDEX), this.REF_VOLUME_VALUE, 'RelTol', 0.001);
        end
        
        function doseVolumeHistogramRelativeTestDto(this)
            dvh = DoseVolumeHistogram(this.refDose, this.voi, this.BIN_SIZE);
            dvh.prescribedDose = this.targetPrescriptionDose;
            verifyEqual(this, dvh.vVolume(this.REF_VOLUME_INDEX), this.REF_VOLUME_VALUE, 'RelTol', 0.001);
            verifyNotEmpty(this, dvh.vDoseRelative);
            verifyNotEmpty(this, dvh.vVolumeRelative);
        end
        
        function calculateDvhDTest(this)
            dvh = DoseVolumeHistogram(this.refDose, this.voi, this.BIN_SIZE);
            dvh.volume = this.voi.volume;
            dvh.prescribedDose = this.targetPrescriptionDose;
            
            out = calculateDvhD(dvh, (this.REF_D_LIMIT_REL/100) * this.voi.volume);
            verifyEqual(this, out, this.REF_D_PARAM, 'RelTol', 0.001);
            out = calculateDvhDRelativeLimit(dvh, this.REF_D_LIMIT_REL);
            verifyEqual(this, out, this.REF_D_PARAM, 'RelTol', 0.001);
            out = calculateRelativeDvhD(dvh, (this.REF_D_LIMIT_REL/100) * this.voi.volume);
            verifyEqual(this, out, this.REF_D_PARAM_REL, 'RelTol', 0.001);
            out = calculateRelativeDvhDRelativeLimit(dvh, this.REF_D_LIMIT_REL);
            verifyEqual(this, out, this.REF_D_PARAM_REL, 'RelTol', 0.001);
        end
        
        function calculateDvhVTest(this)
            dvh = DoseVolumeHistogram(this.refDose, this.voi, this.BIN_SIZE);
            dvh.volume = this.voi.volume;
            dvh.prescribedDose = this.targetPrescriptionDose;
            ref_v_limit_rel = (this.REF_V_LIMIT/this.targetPrescriptionDose) *100;
            
            out = calculateDvhV(dvh, this.REF_V_LIMIT);
            verifyEqual(this, out, this.REF_V_PARAM, 'RelTol', 0.001);
            out = calculateDvhVRelativeLimit(dvh, ref_v_limit_rel);
            verifyEqual(this, out, this.REF_V_PARAM, 'RelTol', 0.001);
            out = calculateRelativeDvhV(dvh, this.REF_V_LIMIT);
            verifyEqual(this, out, this.REF_V_PARAM_REL, 'RelTol', 0.001);
            out = calculateRelativeDvhVRelativeLimit(dvh, ref_v_limit_rel);
            verifyEqual(this, out, this.REF_V_PARAM_REL, 'RelTol', 0.001);
        end
        
        %--- unhappy path ---%
        function doseVolumeHistogramInvalidInputTest(this)
            verifyError(this, @()DoseVolumeHistogram(Image, VolumeOfInterest, 'WrongBinSize'), 'DoseVolumeHistogram:parseImage:InvalidInput');
            verifyError(this, @()DoseVolumeHistogram(double(0), VolumeOfInterest, 0.001), 'DoseVolumeHistogram:parseImage:InvalidInput');
            verifyError(this, @()DoseVolumeHistogram(Image, double(0), 0.001), 'DoseVolumeHistogram:parseImage:InvalidInput');
            dvhError = DoseVolumeHistogram();
            verifyError(this, @()this.setWrongPrescribedDoseTest(dvhError, 'WrongDoseFormat'), 'DoseVolumeHistogram:set_prescribedDose:InvalidInput');
            verifyError(this, @()this.setWrongPrescribedDoseTest(dvhError, [1, 1]), 'DoseVolumeHistogram:set_prescribedDose:InvalidInput');
        end
    end
    
    methods %helper methods because .set functions cannot be tested with the @() annotationa
        function setWrongPrescribedDoseTest(~, dvhError, wrongValue)
            dvhError.prescribedDose = wrongValue;
        end
    end
end