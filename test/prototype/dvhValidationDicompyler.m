classdef dvhValidationDicompyler    
    properties
        referenceCsv;
        testContainer;
        doseImage;
        contours;
        
        RELATIVE_ERROR = 0.01;
    end
    
    methods
        function this = dvhValidationDicompyler()
%             this = this.loadReferenceDvh([findResourcesFolder '/dvh/'], 'dicompyler_reference.csv');
            this = this.loadReferenceDvh('D:\SourceCode\DicomFileInterface\dicom-file-interface\resources\dvh', 'dicompyler_reference.csv');
            this = this.loadDicomData();
        end
        
        function this = loadReferenceDvh(this, path, filename)
            this.referenceCsv = CsvInterface(path, filename);
            this.referenceCsv.delimiter = ';';
            this.referenceCsv =this.referenceCsv.read();
            this.testContainer = containers.Map;
            roisToTest = unique(this.referenceCsv.data(:,1));
            for i = 1:length(roisToTest)
                params = this.referenceCsv.data(strcmp(this.referenceCsv.data(:,1), roisToTest{i}),2:end);
                
                testValues.volume.value = str2double(params{strcmp(params(:,1),'Volume'),4});
                testValues.volume.unit = params{strcmp(params(:,1),'Volume'),5};
                
                dConstrains = params(strcmp(params(:,1),'D'),2:end);
                for j = 1:length(dConstrains)
                    if isnan(str2double(dConstrains{j,1}))
                        testValues.dose.(dConstrains{j,1}) = str2double(dConstrains{j,3});
                    else
                        testValues.(['d_' dConstrains{j,1} '_' dConstrains{j,2}]).value = str2double(dConstrains{j,3});
                        testValues.(['d_' dConstrains{j,1} '_' dConstrains{j,2}]).constrain = str2double(dConstrains{j,1});
                    end
                end
                
                vConstrains = params(strcmp(params(:,1),'V'),2:end);
                for j = 1:length(vConstrains)
                    testValues.(regexprep(['v_' vConstrains{j,1} '_' vConstrains{j,2}],'\.','_')).value = str2double(vConstrains{j,3});
                    testValues.(regexprep(['v_' vConstrains{j,1} '_' vConstrains{j,2}],'\.','_')).constrain = str2double(vConstrains{j,1});
                end
                
                this.testContainer(roisToTest{i}) = testValues;
                clear testValues;
            end
        end
        
        function this = loadDicomData(this)
%             rtdose = RtDose(fullfile(findResourcesFolder, 'maastro', 'RTDOSE.9999.245420512207561792050922341714899059838.dcm'));
            rtdose = RtDose(DicomObj(fullfile('D:\SourceCode\DicomFileInterface\dicom-file-interface\resources\maastro', 'RTDOSE.9999.245420512207561792050922341714899059838.dcm')));
            rtdose = rtdose.readDicomData();
            this.doseImage = createImageFromRtDose(rtdose);
            
%             rtstruct = RtStruct(fullfile(findResourcesFolder, 'maastro', 'RTSTRUCT.9999.70989091007225463773458855903144372742.dcm'));
            rtstruct = RtStruct(DicomObj(fullfile('D:\SourceCode\DicomFileInterface\dicom-file-interface\resources\maastro', 'RTSTRUCT.9999.70989091007225463773458855903144372742.dcm')));

            this.contours = containers.Map;
            roisToTest = unique(this.testContainer.keys);
            for i = 1:length(roisToTest)
                this.contours(roisToTest{i}) = createContour(rtstruct, roisToTest{i});
            end
        end
        
        function tables = testVolume(this)
            keys = this.contours.keys;
            tables = containers.Map;
            for i = 1:length(keys)
                table = {'roi', 'parameter', 'actual', 'referece', 'difference', 'relative difference'};
                voi = createVolumeOfInterest(this.contours(keys{i}),this.doseImage);
                doseVoi = createImageDataForVoi(voi, this.doseImage);
                dvh = DoseVolumeHistogram(doseVoi, 0.01);
                ref = this.testContainer(keys{i});
                
                table = this.addRowToTable(table, 'volume', keys{i}, dvh.volume, ref.volume.value);
                table = this.addRowToTable(table, 'dMax', keys{i}, dvh.maxDose, ref.dose.max);
                table = this.addRowToTable(table, 'dMin', keys{i}, dvh.minDose, ref.dose.min);
                table = this.addRowToTable(table, 'dMean', keys{i}, dvh.meanDose, ref.dose.mean);

                fieldNames = fieldnames(ref);
                for j = 1:length(fieldNames)
                    if ~isempty(regexp(fieldNames{j}, 'd_', 'ONCE'))
                        table = this.addRowToTable(table, fieldNames{j}, keys{i}, calculateDvhD(dvh, ref.(fieldNames{j}).constrain), ref.(fieldNames{j}).value);
                    elseif ~isempty(regexp(fieldNames{j}, 'v_', 'ONCE'))
                        table = this.addRowToTable(table, fieldNames{j}, keys{i}, calculateDvhV(dvh, ref.(fieldNames{j}).constrain), ref.(fieldNames{j}).value);
                    end                        
                end
                tables(keys{i}) = table;
            end
        end
        
        function table = addRowToTable(~, table, parameter, roi, actual, reference)
            row = {roi, parameter,  actual, reference, ...
                    reference-actual, ...
                    ((reference-actual)/reference)*100};
            table = [table; row];
        end
    end 
end
    
