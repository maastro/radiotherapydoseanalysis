%setup logging
delete('integrationTest.log');
logger = logging.getLogger('radiotherapy-dose-analysis');
logger.setLogLevel(logger.DEBUG);
logger.setFilename('integrationTest.log');
clear logger;
logger = logging.getLogger('dicom-file-interface');
logger.setLogLevel(logger.DEBUG);
logger.setFilename('integrationTest.log');
clear logger;

try
%settings
ctPath = './resources/maastro/ct/';
rtstructPath = './resources/maastro/RTSTRUCT.9999.70989091007225463773458855903144372742.dcm';
rtdosePath = './resources/maastro/RTDOSE.9999.245420512207561792050922341714899059838.dcm';
rtplanPath = './resources/maastro/RTPLAN.9999.42722492099431225192788770082885116854.dcm';

roi = {{'GTV-1'},{'Lung_L', 'Lung_R', 'GTV-1'}};
operators = {{''}, {'+','-'}};
binSize = 0.001;

%read data no dose path
ctProperties = mockJavaCtProperties(ctPath);
refImage = createImageFromCtProperties(ctProperties);
rtstruct = createRtStruct(rtstructPath);
dvhGtvNoDose = createDoseVolumeHistogramNoDose( rtstruct, ...
                                                refImage, ...
                                                roi{1}, ...
                                                operators{1});
                                            
dvhHealtyLungsNoDose = createDoseVolumeHistogramNoDose( rtstruct, ...
                                                        refImage, ...
                                                        roi{2}, ...
                                                        operators{2});
%read data dose path
refDose = createReferenceDose(rtdosePath, refImage);

dvhGtv = createDoseVolumeHistogram( rtstruct, ...
                                    refDose, ...
                                    refImage, ...
                                    roi{1}, ...
                                    operators{1}, ...
                                    binSize);

dvhHealtyLungs = createDoseVolumeHistogram( rtstruct, ...
                                            refDose, ...
                                            refImage, ...
                                            roi{2}, ...
                                            operators{2}, ...
                                            binSize);
                                
disp('Integration test completed succesfull!')
catch exception
    disp('Integration test failed :<')
    disp(getReport(exception, 'extended'));
end