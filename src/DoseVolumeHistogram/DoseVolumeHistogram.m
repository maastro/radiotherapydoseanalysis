classdef DoseVolumeHistogram
% DOSEVOLUMEHISTOGRAM converts the data of a VolumeOfInterest clipped dose Image to a sampled
%  DoseVolumeHistogram
%
% CONSTRUCTOR
%  this = DoseVolumeHistogram(image, binsize) samples an image object with the size of binsize
%
% See also: VOLUMEOFINTEREST, CALCULATEDVHD, CALCULATEDVHV, CREATEDOSEVOLUMEHISTOGRAMDTO,
% CREATEDOSEVOLUMEHISTOGRAMFROMDTO
    properties
        prescribedDose
        volume
        binsize
        vVolume
        vDose
        doseSamples
        vVolumeRelative
        vDoseRelative
    end
    
    methods
        function this = DoseVolumeHistogram(doseImage, voi, binsize)
            if nargin == 0
                return;
            end
            
            this = this.parseImage(doseImage, voi, binsize);
        end
        
        function this = parseImage(this, doseImage, voi, binsize)
        %PARSEIMAGE(doseImage, voi, binsize) converts a provided dose image and bitmask to a DoseVolumeHistogram
        % in the provided binsize.
            if ~isnumeric(binsize) || length(binsize) ~= 1
                throw(MException('DoseVolumeHistogram:parseImage:InvalidInput','binsize should be single numeric value'));
            end
            
            if ~isa(voi, 'VolumeOfInterest') || ~isa(doseImage, 'Image')
                throw(MException('DoseVolumeHistogram:parseImage:InvalidInput','voi and image should be valid Image objects'));
            end
            
            this.binsize = binsize;
            this.volume = voi.volume;
            
            if isempty(doseImage.pixelData)
                return;
            end
            
            voiDose = createImageDataForVoi(voi, doseImage);
            this.vDose = this.calculateDoseVector(this.calculateSortedDose(voiDose), binsize);
            this.vVolume = this.calculateVolumeVector(voiDose);
        end

        % -------- START GETTERS/SETTERS ----------------------------------        
        function out = get.doseSamples(this)
            out = length(this.vDose);
        end
        
        function out = get.vVolumeRelative(this)
            out = [];
            if ~isempty(this.volume)
                out = (this.vVolume / this.volume) * 100;
            end
        end
        
        function out = get.vDoseRelative(this)
            out = [];
            if ~isempty(this.prescribedDose)
                out = (this.vDose / this.prescribedDose) * 100;
            end
        end
        
        function this = set.prescribedDose(this, dose)
            if ~isnumeric(dose) || length(dose) ~= 1
                throw(MException('DoseVolumeHistogram:set_prescribedDose:InvalidInput','prescribedDose should be single numeric value'));
            end
            this.prescribedDose = dose;
        end
    end
    
    methods (Access = protected)
        function out = calculateSortedDose(~, voiDose)
            out = 0;
            if ~isempty(voiDose.pixelData)
                out = voiDose.pixelData(~isnan(voiDose.pixelData));
                out = sort(out(:), 'ascend');
            end
        end
        
        function out = calculateDoseVector(~, sortedDose, binsize)
            out = [];
            if ~isempty(sortedDose)
                out = (0:binsize:(sortedDose(size(sortedDose,1)) + binsize));
            end
        end
        
        function out = calculateVolumeVector(this, voiDose)
            out = 0;
            if ~isempty(voiDose.pixelData)
                vHistogram = histcounts(this.calculateSortedDose(voiDose), this.vDose,'Normalization', 'cumcount');
                vHistogram = [0, vHistogram]; %need to ad a leading 0 to make sure the vector is alligned with the dose vector
                out = abs(vHistogram - max(vHistogram)); % Inverse
                out = out .* (voiDose.pixelSpacingX * voiDose.pixelSpacingY * voiDose.pixelSpacingZ);
            end
        end
    end
end