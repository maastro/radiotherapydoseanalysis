function out = calculateRelativeDvhD(dvh, volumeLimit)
%CALCULATERELATIVEDVHD calculate the dose volume histogram d-parameter (in % prescribed dose) for the given volume limit in %
%
% out = calculateRelativeDvhD(dvh, volumeLimit) where dvh is a DoseVolumeHistogram object and volume
%   limit (cc)
%
% See also: DOSEVOLUMEHISTOGRAM, CALCULATEDVHD, CALCULATEDVHDRELATIVELIMIT, CALCULATERELATIVEDVHDRELATIVELIMIT
    if isempty(dvh.prescribedDose)
        throw(MException('calculateRelativeDvhD:ProcessingError', ...
            'cannot calculate relative dose volume histgram D parameter because the prescribed dose is not set.'))
    end

    out = calculateDvhD(dvh, volumeLimit);
    out = out / dvh.prescribedDose * 100;
end

