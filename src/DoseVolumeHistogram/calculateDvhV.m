function out = calculateDvhV(dvh, doseLimit)
%CALCULATEDVHV  calculate the dose volume histogram v-parameter (in cc) for the given dose limit
%
% out = calculateDvhV(dvh, doseLimit) where dvh is a DoseVolumeHistogram object and dose
%   limit (Gy)
%
% See also: DOSEVOLUMEHISTOGRAM, CALCULATEDVHVRELATIVELIMIT, CALCULATERELATIVEDVHV, CALCULATERELATIVEDVHVRELATIVELIMIT

    index = find((dvh.vDose(:) >= doseLimit), 1, 'first');
    out = dvh.vVolume(index);
    
    if isempty(out)
        out = 0;
    end
end