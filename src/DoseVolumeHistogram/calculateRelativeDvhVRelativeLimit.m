function out = calculateRelativeDvhVRelativeLimit(dvh, doseLimit)
%CALCULATERELATIVEDVHVRELATIVELIMIT  calculate the dose volume histogram v-parameter (in %) for the given dose limit
%
% out = calculateDvhV(dvh, doseLimit) where dvh is a DoseVolumeHistogram object and dose
%   limit (% prescribed dose)
%
% See also: DOSEVOLUMEHISTOGRAM, CALCULATEDVHV, CALCULATEDVHVRELATIVELIMIT, CALCULATERELATIVEDVHV, CALCULATERELATIVEDVHVRELATIVELIMIT
    if isempty(dvh.prescribedDose)
        throw(MException('calculateRelativeDvhVRelativeLimit:ProcessingError', ...
            'cannot calculate relative dose volume histgram V parameter because the prescribed dose is not set.'))
    end
    
    doseLimit = (doseLimit/100) * dvh.prescribedDose;
    out = calculateRelativeDvhV(dvh, doseLimit);
end

