function out = calculateRelativeDvhDRelativeLimit(dvh, volumeLimit)
%CALCULATERELATIVEDVHDRELATIVELIMIT calculate the dose volume histogram d-parameter (in % prescribed dose) for the given volume limit in %
%
% out = calculateDvhDRelativeLimit(dvh, volumeLimit) where dvh is a DoseVolumeHistogram object and volume
%   limit is a double between 0 and 100 %
%
% See also: DOSEVOLUMEHISTOGRAM, CALCULATEDVH, CALCULATERELATIVEDVHD, CALCULATEDVHDRELATIVELIMIT    
    volumeLimit = dvh.volume*(volumeLimit/100);
    out = calculateRelativeDvhD(dvh, volumeLimit)';   
end

