function out = calculateRelativeDvhV(dvh, doseLimit)
%CALCULATERELATIVEDVHV  calculate the dose volume histogram v-parameter (in %) for the given dose limit
%
% out = calculateDvhV(dvh, doseLimit) where dvh is a DoseVolumeHistogram object and dose
%   limit (Gy)
%
% See also: DOSEVOLUMEHISTOGRAM, CALCULATEDVHV, CALCULATEDVHVRELATIVELIMIT, CALCULATERELATIVEDVHV, CALCULATERELATIVEDVHVRELATIVELIMIT

    out = calculateDvhV(dvh, doseLimit);
    out = ((out / dvh.volume) * 100);
end