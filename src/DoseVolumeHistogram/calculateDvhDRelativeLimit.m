function out = calculateDvhDRelativeLimit(dvh, volumeLimit)
%CALCULATEDVHDRELATIVELIMIT  calculate the dose volume histogram d-parameter (in Gy) for the given volume limit in %
%
% out = calculateDvhDRelativeLimit(dvh, volumeLimit) where dvh is a DoseVolumeHistogram object and volume
%   limit is a double between 0 and 100 %
%
% See also: DOSEVOLUMEHISTOGRAM, CALCULATEDVH, CALCULATERELATIVEDVHD, CALCULATERELATIVEDVHDRELATIVELIMIT

    volumeLimit = dvh.volume*(volumeLimit/100);
    out = calculateDvhD(dvh, volumeLimit);
end

