function out = calculateDvhD(dvh, volumeLimit)    
%CALCULATEDVHD calculate the dose volume histogram d-parameter (in Gy) for the given volume limit in cc
%
% out = calculateDvhD(dvh, volumeLimit) where dvh is a DoseVolumeHistogram object and volume
%   limit (cc)
%
% See also: DOSEVOLUMEHISTOGRAM, CALCULATERELATIVEDVHD, CALCULATEDVHDRELATIVELIMIT, CALCULATERELATIVEDVHDRELATIVELIMIT

    out = dvh.vDose(find(dvh.vVolume <= volumeLimit, 1,'first'));
    %take the first one because they all have the dose criteria
    
    if isempty(out)
        out = 0;
    end
end