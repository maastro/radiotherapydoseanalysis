%CREATERTSTRUCT provides Java with the necessary RTSTRUCT object.
function [rtStruct] = createRtStruct(pathStruct)    
    rtStruct = RtStruct(DicomObj(pathStruct));

    logger = logging.getLogger('radiotherapy-dose-analysis');
    logger.info('finished reading rtstruct');
end