function image = createImageFromCtProperties(ctProperties)
%CREATEIMAGEFROMCTPROPERTIES creates an Image object using a struct of ctProperties
%
% image = createImageFromCtProperties(ctProperties) is used to construct a grid without having to
%  read the dicom files in matlab to save speed when using this library from java. This function
%  converts the following CT dicom tags into the information that is needed to construct the image:
% + (0018,0050) SliceThickness
% + (0020,0032) ImagePositionPatient
% + (0020,0037) ImageOrientationPatient
% + (0028,0010) Rows
% + (0028,0011) Columns
% + (0028,0030) PixelSpacing
% + CTFileLength this is not a dicom TAG but the amound of slices in the CT scan
%
% See also: CTSCAN, CTSLICE, CREATEIMAGEFROMCT

validateCtProperties(ctProperties);

names = fieldnames(ctProperties);
for i = 1:length(names)
    ctProperties.(names{i}) = convertToDouble(ctProperties.(names{i}));
end

IEC_MM_TO_CM = 10;
image = Image();

originX = [];
originY = ctProperties.ImagePositionPatient(3)/IEC_MM_TO_CM;
originZ = [];

image.pixelSpacingX = ctProperties.PixelSpacing(1)/IEC_MM_TO_CM;
image.pixelSpacingY = ctProperties.SliceThickness/IEC_MM_TO_CM;
image.pixelSpacingZ = ctProperties.PixelSpacing(2)/IEC_MM_TO_CM;
imagePositionPatientX = ctProperties.ImagePositionPatient(1)/IEC_MM_TO_CM;
imagePositionPatientZ = ctProperties.ImagePositionPatient(2)/IEC_MM_TO_CM;

if ctProperties.ImageOrientationPatient(1) == 1
    originX = imagePositionPatientX;
elseif ctProperties.ImageOrientationPatient(1) == -1
    originX = imagePositionPatientX - ...
        (image.pixelSpacingX * ctProperties.Columns);
end
if ctProperties.ImageOrientationPatient(5) == -1
    originZ = -imagePositionPatientZ;
elseif ctProperties.ImageOrientationPatient(5) == 1
    originZ = -imagePositionPatientZ - ...
        (image.pixelSpacingZ * (ctProperties.Rows - 1));
end

image.realX = (originX : image.pixelSpacingX : ...
    (originX + ...
    (double(ctProperties.Columns) - 1) * image.pixelSpacingX))';
image.realY = (originY : image.pixelSpacingY : ...
    (originY + ...
    (ctProperties.CTFileLength - 1) * image.pixelSpacingY))';
image.realZ = (originZ : image.pixelSpacingZ : ...
    (originZ + ...
    (ctProperties.Rows - 1) * image.pixelSpacingZ))';
end

function doubleNumber = convertToDouble(number)
if(~isa(number,'double'))
    doubleNumber = double(number);
else
    doubleNumber = number;
end
end

function validateCtProperties(ctProperties)
    message = '';
    if ~isfield(ctProperties,'ImagePositionPatient')
        message = [message 10 'Missing field for ctProperties: ImagePositionPatient'];
    elseif ~isnumeric(ctProperties.ImagePositionPatient) || ~(size(ctProperties.ImagePositionPatient,2) == 3)
        message = [message 10 'Invalid data for ctProperties: ImagePositionPatient, please provide numeric array'];
    end
    if ~isfield(ctProperties,'PixelSpacing')
        message = [message 10 'Missing field for ctProperties: PixelSpacing'];
    elseif ~isnumeric(ctProperties.PixelSpacing) || ~(size(ctProperties.PixelSpacing,2) == 2)
        message = [message 10 'Invalid data for ctProperties: PixelSpacing, please provide numeric array'];
    end

    if ~isfield(ctProperties,'ImageOrientationPatient')
        message = [message 10 'Missing field for ctProperties: ImageOrientationPatient'];
    elseif ~isnumeric(ctProperties.ImageOrientationPatient) || ~(size(ctProperties.ImageOrientationPatient,2) == 6)
        message = [message 10 'Invalid data for ctProperties: ImageOrientationPatient, please provide numeric array'];
    end
    
    if ~isfield(ctProperties,'SliceThickness')
        message = [message 10 'Missing field for ctProperties: SliceThickness'];
    elseif ~isnumeric(ctProperties.SliceThickness)
        message = [message 10 'Invalid data for ctProperties: SliceThickness, please provide single numeric'];
    end
    
    if ~isfield(ctProperties,'Rows')
        message = [message 10 'Missing field for ctProperties: Rows'];
    elseif ~isnumeric(ctProperties.Rows)
        message = [message 10 'Invalid data for ctProperties: Rows, please provide single numeric'];
    end
    
    if ~isfield(ctProperties,'Columns')
        message = [message 10 'Missing field for ctProperties: Columns'];
    elseif ~isnumeric(ctProperties.Columns)
        message = [message 10 'Invalid data for ctProperties: Columns, please provide single numeric'];
    end

    if ~isempty(message)
        message = ['Validation of CtProperties structure failed' message];
        throw(MException('createImageFromCtProperties:validateCtProperties:InvalidInput', message));
    end
end