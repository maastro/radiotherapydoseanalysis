%CREATEREFERENCEDOSE provides Java with the necessary dosereference to perform
%   calculations with CALCULATESTRUCTEDATAWRAPPER. To shorten computation
%   times this load was split from the calculations
%
%   See also ROIDOSE, RTDOSE, CT, CALCULATIONGRID.
function referenceDose = createReferenceDose(pathDose, targetPrescriptionDose, referenceImage)    
    rtdose = createRtDose(pathDose);

    if strcmpi(rtdose.doseUnits, 'relative')
        doseImage = createImageFromRelativeRtDose(rtdose, targetPrescriptionDose);
    else
        doseImage = createImageFromRtDose(rtdose);
    end
    
    referenceDose = matchImageRepresentation(doseImage, referenceImage);
    
    logger = logging.getLogger('radiotherapy-dose-analysis');
    logger.info('finished converting rtdose pixel data to Image on referenceImage grid');
end