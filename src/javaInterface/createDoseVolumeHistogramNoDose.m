%CREATEDOSEVOLUMEHISTOGRAMNODOSE   Create empty dose-volume histogram.
%
%   DVH = CREATEDOSEVOLUMEHISTOGRAMNODOSE(RTSTRUCT, REFERENCEIMAGE, ROIS,
%   OPERATORS) creates an empty dose-volume histogram for a given volume of
%   interest.  The volume of interest is extracted from the RT-Struct by 
%   combining the ROIs using the specified operators. The output is a JSON 
%   string.
% 
%   See also CREATEDOSEVOLUMEHISTOGRAM, CREATEDOSEVOLUMEHISTOGRAMUSINGVOINODOSE.

function dvhJson = createDoseVolumeHistogramNoDose(rtStruct, ... 
                                             referenceImage, ...
                                             roiNames, ...
                                             operators) 
    try
        combinedVoi = combineVois( rtStruct, roiNames, operators, referenceImage );
    catch exception
        warning('createDoseVolumeHistogramNoDose:ProcessingError', 'Unable to create volume of interest');
        rethrow(exception);
    end
    
    dvhJson = createDoseVolumeHistogramUsingVoiNoDose(combinedVoi);
    
end