%CREATECTSCAN provides Java with the necessary CTSCAN object.
function [ctScan] = createCtScan(ctSlicePaths)    
    if ~isa(ctSlicePaths,'cell')
        throw(MException('createCtScan:InvalidInput', 'The provided paths are not a valid cell array'));
    end
    ctScan = CtScan();
    ctScan = ctScan.addCellArrayOfFiles(ctSlicePaths);
    ctScan = ctScan.readDicomData();
    
    logger = logging.getLogger('radiotherapy-dose-analysis');
    logger.info('finished reading ct data');
end