%CREATERTDOSE provides Java with the necessary RTDOSE object.
function rtdose = createRtDose(pathDose)
    rtdose = RtDose(DicomObj(pathDose));
    rtdose = rtdose.readDicomData();
    
    logger = logging.getLogger('radiotherapy-dose-analysis');
    logger.info('finished reading rtdose');
end