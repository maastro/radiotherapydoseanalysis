%CREATEDOSEVOLUMEHISTOGRAM   Create dose-volume histogram.
%
%   DVH = CREATEDOSEVOLUMEHISTOGRAM(RTSTRUCT, REFERENCEDOSE, REFERENCEIMAGE, 
%   ROIS, OPERATORS, BINSIZE) creates a dose-volume histogram for a
%   given volume of interest and RT-Dose using the specified binSize. The
%   volume of interest is extracted from the RT-Struct by combining the
%   ROIs using the specified operators. The output is a JSON string.
% 
%   See also CREATEDOSEVOLUMEHISTOGRAMUSINGVOI.

function dvhJson = createDoseVolumeHistogram(rtStruct, ... 
                                             referenceDose, ...
                                             referenceImage, ...
                                             roiNames, ...
                                             operators, ...
                                             binSize) 
    try
        combinedVoi = combineVois(rtStruct, roiNames, operators, referenceImage);
    catch exception
        warning('createDoseVolumeHistogram:ProcessingError', 'Unable to create volume of interest');
        rethrow(exception);
    end
    
    dvhJson = createDoseVolumeHistogramUsingVoi(combinedVoi, referenceDose, binSize);
    
end