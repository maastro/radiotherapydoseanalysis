%CREATEDOSEVOLUMEHISTOGRAMUSINGVOINODOSE   Create empty dose-volume histogram.
%
%   DVH = CREATEDOSEVOLUMEHISTOGRAMUSINGVOINODOSE(VOI) creates an empty 
%   dose-volume histogram for a given volume of interest. The 
%   output is a JSON string.
% 
%   See also CREATEDOSEVOLUMEHISTOGRAMUSINGVOI, CREATEDOSEVOLUMEHISTOGRAMNODOSE.

function dvhJson = createDoseVolumeHistogramUsingVoiNoDose(voi)

    try
        dvh = DoseVolumeHistogram(Image(), voi, 0.001);
        dvhJson = createJavaInterfaceDto(dvh, Image(), VolumeOfInterest);
    catch exception
        warning('createDoseVolumeHistogramUsingVoiNoDose:ProcessingError', 'Unable to create dose-volume histogram');
        rethrow(exception);
    end
    
end
