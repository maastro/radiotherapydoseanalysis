%CREATEDOSEVOLUMEHISTOGRAMUSINGVOI   Create dose-volume histogram.
%
%   DVH = CREATEDOSEVOLUMEHISTOGRAMUSINGVOI(VOI, DOSEIMAGE, BINSIZE) creates
%   a dose-volume histogram for a given volume of interest and RT-Dose 
%   using the specified binSize. The output is a JSON string.
% 
%   See also CREATEDOSEVOLUMEHISTOGRAM.

function dvhJson = createDoseVolumeHistogramUsingVoi(voi, doseImage, binSize)
    
    try
        dvh = DoseVolumeHistogram(doseImage, voi, binSize);
        dvhJson = createJavaInterfaceDto(dvh, doseImage, voi);
    catch exception
        warning('createDoseVolumeHistogramUsingVoi:ProcessingError', 'Unable to create dose-volume histogram');
        rethrow(exception);
    end
    
end
