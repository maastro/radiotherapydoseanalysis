function dto = createJavaInterfaceDto( dvh, doseImage, voi)
    out.volumeUnit = 'cc';
    out.doseUnit = 'Gy';
    
    out.volume = dvh.volume;
    if ~isempty(doseImage.pixelData)
        voiDose = createImageDataForVoi(voi, doseImage);
        out.minDose = calculateImageStatistics(voiDose, 'min');
        out.meanDose = calculateImageStatistics(voiDose, 'mean');
        out.maxDose = calculateImageStatistics(voiDose, 'max');
    else
        out.minDose = [];
        out.meanDose = [];
        out.maxDose = [];
    end
    
    out.vVolume = dvh.vVolume;
    out.vDose = dvh.vDose;
    out.binSize = dvh.binsize;

    dto = savejson('',out);
end

