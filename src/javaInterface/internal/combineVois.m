function combinedVoi = combineVois( rtStruct, roiNames, operators, referenceImage )
    contour = createContour(rtStruct, roiNames{1});
    voi = createVolumeOfInterest(contour, referenceImage);
    combinedVoi = voi;

    for i = 2:length(roiNames)
        contour = createContour(rtStruct, roiNames{i});
        voi = createVolumeOfInterest(contour, referenceImage);
        if(strcmp(operators{i-1},'+'))
            combinedVoi = combinedVoi + voi;
        elseif(strcmp(operators{i-1},'-'))
            combinedVoi = combinedVoi - voi;
        else
            throw(MException('combineVois:ProcessingError',['operator not recognized: ' operators{i-1}]));
        end
    end
end