function dependencies = setup()
    dependencies(1) = DependencyContainer('https://bitbucket.org/maastrosdt/dicomfileinterface.git');
    dependencies(2) = DependencyContainer('https://github.com/TimLustberg/jsonlab');
    dependencies(3) = DependencyContainer('https://github.com/TimLustberg/logging4matlab');
end

